var gulp = require('gulp'),
    concat = require('gulp-concat'),//склеейка файлов
    rename = require('gulp-rename'),//переименование файлов по ходу обработки

    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,

    path = require('path'),
    url_adj = require('gulp-css-url-adjuster'), //изменение урлов для картинок в CSS

    autoprefixer = require('autoprefixer-core'),//добавляет вендорные префиксы, где необходимо
    postcss = require('gulp-postcss');

var params = {
    out: 'public',
    htmlSrc: 'dark.index.html',
    levels: ['common.blocks', 'dark.blocks']
};

gulp.task('default', ['server', 'build']);

gulp.task('server', function(){
    browserSync.init({
        server:params.out
    });

    gulp.watch('*.html', ['html']);
});

gulp.task('build',['html', 'css', 'images']);

gulp.task('html', function () {
    gulp.src(params.htmlSrc)
        .pipe(rename('index.html'))//назовем файлом index
        .pipe(gulp.dest(params.out))//сохраним в нужную папку
        .pipe(reload({ stream:true })); //обновим браузер;
});

gulp.task('css', function () {
    gulp.src(['common.blocks/**/*.css', 'dark.blocks/**/*.css'])
        .pipe(concat('dark.styles.css')) //склеим в файл style.css
        .pipe(postcss( [autoprefixer({
                browsers: ['last 3 versions'],
                cascade: false
            })] ))
        .pipe(url_adj({ prepend:'images/' })) //подправим пути к картинкам в стилях - добавим путь к началу
        .pipe(gulp.dest(params.out)) //сохраним в нужную папку
        .pipe(reload({ stream:true })); //обновим браузер
});

gulp.task('images', function () {
    gulp.src(['common.blocks/**/*.jpg', 'common.blocks/**/*.png' , 'common.blocks/**/*.svg',
            'dark.blocks/**/*.jpg', 'dark.blocks/**/*.png', 'dark.blocks/**/*.svg'])
        .pipe(rename(function(path){
            path.dirname = '';
            //path.basename += "-goodbye";
            //.extname, prefix, suffix, extname: ".jpg"
        }))
        .pipe(gulp.dest(path.join(params.out, 'images')))
        .pipe(reload({ stream: true }));
})
    